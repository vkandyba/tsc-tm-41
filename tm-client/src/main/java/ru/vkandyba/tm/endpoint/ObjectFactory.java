
package ru.vkandyba.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.vkandyba.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "createUserResponse");
    private final static QName _LockUser_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "lockUser");
    private final static QName _LockUserResponse_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "lockUserResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "removeUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "removeUserResponse");
    private final static QName _UnLockUser_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "unLockUser");
    private final static QName _UnLockUserResponse_QNAME = new QName("http://endpoint.tm.vkandyba.ru/", "unLockUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.vkandyba.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link LockUser }
     * 
     */
    public LockUser createLockUser() {
        return new LockUser();
    }

    /**
     * Create an instance of {@link LockUserResponse }
     * 
     */
    public LockUserResponse createLockUserResponse() {
        return new LockUserResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link UnLockUser }
     * 
     */
    public UnLockUser createUnLockUser() {
        return new UnLockUser();
    }

    /**
     * Create an instance of {@link UnLockUserResponse }
     * 
     */
    public UnLockUserResponse createUnLockUserResponse() {
        return new UnLockUserResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "lockUser")
    public JAXBElement<LockUser> createLockUser(LockUser value) {
        return new JAXBElement<LockUser>(_LockUser_QNAME, LockUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "lockUserResponse")
    public JAXBElement<LockUserResponse> createLockUserResponse(LockUserResponse value) {
        return new JAXBElement<LockUserResponse>(_LockUserResponse_QNAME, LockUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnLockUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnLockUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "unLockUser")
    public JAXBElement<UnLockUser> createUnLockUser(UnLockUser value) {
        return new JAXBElement<UnLockUser>(_UnLockUser_QNAME, UnLockUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnLockUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnLockUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vkandyba.ru/", name = "unLockUserResponse")
    public JAXBElement<UnLockUserResponse> createUnLockUserResponse(UnLockUserResponse value) {
        return new JAXBElement<UnLockUserResponse>(_UnLockUserResponse_QNAME, UnLockUserResponse.class, null, value);
    }

}
