package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class ProjectRemoveWithTasksByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "remove-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().removeProjectWithTasksById(session, projectId);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
