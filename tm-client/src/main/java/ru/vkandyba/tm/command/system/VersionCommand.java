package ru.vkandyba.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version...";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getVersion());
    }

}
