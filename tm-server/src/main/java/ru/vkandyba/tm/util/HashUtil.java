package ru.vkandyba.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IPropertyService;

import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final IPropertyService propertyService,
            @Nullable final Object value
    ) throws JsonProcessingException {
        if (propertyService == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(value);
        @Nullable final String secret = propertyService.getHashSecret();
        @Nullable final Integer iteration = Integer.parseInt(propertyService.getHashIteration());
        return salt(secret, iteration, json);
    }

    static String salt(@NotNull IPropertyService propertyService,@Nullable String value){
        @Nullable final String SECRET = propertyService.getHashSecret();
        @Nullable final Integer ITERATION = Integer.parseInt(propertyService.getHashIteration());
        return salt(SECRET, ITERATION, value);
    }

    static String salt( @Nullable final String secret, @Nullable final Integer iteration,
                        @Nullable final String value)
    {
        if (value == null) return null;
        if (secret == null) return null;
        if (iteration == null) return null;
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    static String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    static String sign(final IPropertyService propertyService, final Object value){
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(value);
        @Nullable final String secret = propertyService.getSignSecret();
        @Nullable final Integer iteration = Integer.parseInt(propertyService.getSignIteration());
        return salt(secret, iteration, json);
    }

}
