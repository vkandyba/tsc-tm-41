package ru.vkandyba.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.dto.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity>{

    @NotNull
    IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
