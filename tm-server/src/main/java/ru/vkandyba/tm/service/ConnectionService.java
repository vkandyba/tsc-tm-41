package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ISessionRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IPropertyService;
import ru.vkandyba.tm.dto.Project;
import ru.vkandyba.tm.dto.Session;
import ru.vkandyba.tm.dto.Task;
import ru.vkandyba.tm.dto.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = factory();
    }

    public SqlSession getSqlSession(){
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final String driver = propertyService.getJdbcDriver();
        @NotNull final String username = propertyService.getJdbcUser();
        @NotNull final String password = propertyService.getJdbcPassword();
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final String dialect = propertyService.getHibernateDialect();
        @NotNull final String auto = propertyService.getHibernateBM2DDLAuto();
        @NotNull final String showSql = propertyService.getHibernateShowSql();

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, showSql);

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    private SqlSessionFactory getSqlSessionFactory(){
        @NotNull final String driver = propertyService.getJdbcDriver();
        @NotNull final String username = propertyService.getJdbcUser();
        @NotNull final String password = propertyService.getJdbcPassword();
        @NotNull final String url = propertyService.getJdbcUrl();

        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    public IPropertyService getPropertyService() {
        return propertyService;
    }
}
