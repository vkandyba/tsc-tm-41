package ru.vkandyba.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.vkandyba.tm.enumerated.Role;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_users")
public class User extends AbstractEntity {

    @Column
    private String login;

    @Column(name = "password")
    private String passwordHash;

    @Column
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private Boolean locked = false;

}
