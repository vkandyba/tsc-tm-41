package ru.vkandyba.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.dto.Session;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session open(String login, String password);

    void close(Session session);

    boolean checkDataAccess(String login, String password);

    void validate(Session session);

    void validate(Session session, Role role);

    @Nullable
    Session sign(Session session);

}
