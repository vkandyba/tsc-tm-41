package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IUserEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.Session;
import ru.vkandyba.tm.dto.User;

import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable User viewUserInfo(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

    @Override
    public void changeUserPassword(@NotNull Session session, @NotNull String password) {
        serviceLocator.getSessionService().validate(session);
//        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    public void updateUserProfile(@NotNull Session session, @NotNull String firstName, @NotNull String lastName, @NotNull String midName) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUser(session.getUserId(), firstName, lastName, midName);
    }

}
